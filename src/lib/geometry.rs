use std::{f64::consts::*, vec::IntoIter};

#[derive(Copy, Clone, Debug, Default)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Rect {
    pub min: Point,
    pub max: Point,
}

impl Rect {
    pub fn contains(self, p: &Point) -> bool {
        p.x >= self.min.x && p.y >= self.min.y && p.x <= self.max.x && p.y <= self.max.y
    }
}

#[derive(Clone, Debug)]
pub struct View {
    bounds: Rect,
    path: Path,
}

impl View {
    pub fn new(path: Path, bounds: Rect) -> Self {
        Self { path, bounds }
    }

    pub fn path(self, pb: &Rect) -> Path {
        let b = self.bounds;
        let tx_x = (b.max.x - b.min.x) / 2.0;
        let tx_y = (b.max.y - b.min.y) / 2.0;

        let points = self
            .path
            .points
            .iter()
            .map(|p| Point {
                x: pb.min.x + b.min.x + tx_x + tx_x * p.x,
                y: pb.min.y + b.min.y + tx_y + tx_y * p.y,
            })
            .collect::<Vec<Point>>();

        Path { points }
    }
}

/// TODO: automatic layouts based on method/style & count of views. For now, manual layout.
#[derive(Clone, Debug, Default)]
pub struct Layout {
    views: Vec<View>,
}

impl Layout {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn push(&mut self, v: View) {
        self.views.push(v);
    }
}

impl IntoIterator for Layout {
    type Item = View;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.views.into_iter()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct HarmonographParameters {
    /// How many points to draw.
    /// TODO: find an alg to automatically calculate a value that results in smooth lines.
    pub n: u64,

    /// Minimum value to plot from.
    pub t_min: f64,

    /// Maximum value to plot to.
    pub t_max: f64,

    /// Decay constant.
    pub d: f64,

    pub f1: f64,
    pub p1: f64,
    pub f2: f64,
    pub p2: f64,
}

impl Default for HarmonographParameters {
    fn default() -> Self {
        Self {
            n: 8000,
            d: 0.39,
            t_min: 0.0,
            t_max: 100.0 * TAU,
            f1: 0.933,
            f2: 0.62347,
            p1: 2.17,
            p2: 0.0,
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Path {
    pub points: Vec<Point>,
}

impl Path {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn generate(p: &HarmonographParameters) -> Path {
        //Self::generate_1(p)
        Self::generate_2(p)
    }

    #[allow(dead_code)]
    fn generate_1(p: &HarmonographParameters) -> Path {
        let mut path = Path::new();
        let mut t = p.t_min;

        let dt = (p.t_max - p.t_min) / 40000.0;

        // This exists as a way to be able to scale the decay by some amount to make it more friendly to use by a
        // human. Based on some totally subjective evaluations of plots this value was settled on so the curves would be
        // "pleasantly dense" when d has the value 0.5.
        let k = p.d.powi(7);

        while t <= p.t_max {
            let x = (t * p.f1 + p.p1).sin() * E.powf(-1.0 * k * t);
            let y = (t * p.f2 + p.p2).sin() * E.powf(-1.0 * k * t);

            path.points.push(Point { x, y });

            t += dt;
        }

        path
    }

    fn generate_2(p: &HarmonographParameters) -> Path {
        let n = 5.0;
        let o = 60.0;
        let mut path = Path::new();
        let mut t = p.t_min;

        // This exists as a way to be able to scale the decay by some amount to make it more friendly to use by a
        // human. Based on some totally subjective evaluations of plots this value was settled on so the curves would be
        // "pleasantly dense" when d has the value 0.5.
        let k = p.d.powi(7);

        while t <= p.t_max {
            let x = (t * p.f1 + p.p1).sin() * E.powf(-1.0 * k * t);
            let y = (t * p.f2 + p.p2).sin() * E.powf(-1.0 * k * t);

            path.points.push(Point { x, y });

            let hx = (E.powf(-1.0 * k * t)
                * ((k * k - p.f1 * p.f1) * (t * p.f1 + p.p1).sin()
                    - 2.0 * k * p.f1 * (t * p.f1 + p.p1).cos()))
            .abs();
            let ax = TAU / (n * (1.0 + o + hx));

            let hy = (E.powf(-1.0 * k * t)
                * ((k * k - p.f2 * p.f2) * (t * p.f2 + p.p2).sin()
                    - 2.0 * k * p.f2 * (t * p.f2 + p.p2).cos()))
            .abs();
            let ay = TAU / (n * (1.0 + o + hy));

            t += (ax + ay) / 2.0;
        }

        path
    }
}
