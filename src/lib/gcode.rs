use std::fmt::{ self, Display };

use crate::geometry::{ Layout, Path, Point, Rect };

static DEFAULT_HEADER: &str = r#"
M402 ; retract pen
G90 ; use absolite coordinates
G28 ; home all
G21 ; units in mm
G90 ; use absolite coordinates
"#;

static DEFAULT_FOOTER: &str = r#"
M402 ; retract pen
G28 ; home all
M84 ; turn off all steper drivers
"#;

#[derive(Clone, Debug)]
pub struct GCode {
    lines: Vec<String>,
}

// TODO: Add validation.
impl GCode {
    fn new() -> Self {
        Self {
            lines: vec![],
        }
    }

    fn push<S>(&mut self, s: S) 
    where S: AsRef<str> {
        let line = s.as_ref().to_string();
        // TODO: add validation?
        self.lines.push(line);
    }

    fn extend(&mut self, g: GCode) {
        self.lines.extend(g.lines);
    }

    // TODO: add validation function
}

impl From<Vec<String>> for GCode {
    fn from(lines: Vec<String>) -> Self {
        GCode { lines }
    }
}

impl From<&'static str> for GCode {
    fn from(s: &'static str) -> Self {
        s.trim()
            .split("\n")
            .map(|s| s.to_string())
            .collect::<Vec<String>>()
            .into()
    }
}

impl Display for GCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       write!(f, "{}", self.lines.join("\n"))
    }
}

#[derive(Clone, Debug)]
enum PenState {
    Unknown,
    Down,
    Up,
}

#[derive(Clone, Debug)]
struct PlotterState {
    feed_rate: Option<u64>,
    location: Option<Point>,
    pen: PenState,
}

impl PlotterState {
    fn new() -> Self {
        Self {
            feed_rate: None,
            location: None,
            pen: PenState::Unknown,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Plotter {
    pub min_feed_rate: u64,
    pub max_feed_rate: u64,
    pub bounds: Rect,

    state: PlotterState,
}

impl Plotter {
    pub fn new(min_feed_rate: u64, max_feed_rate: u64, bounds: Rect) -> Self {
        Self {
            min_feed_rate,
            max_feed_rate,
            bounds,
            state: PlotterState::new(),
        }
    }
}

impl Default for Plotter {
    fn default() -> Self {
        Self {
            min_feed_rate: 60, // 1mm/s
            max_feed_rate: 12000, // 200 mm/s
            // A0 Paper
            bounds: Rect{
                min: Point{x: 0.0, y: 0.0},
                max: Point{x: 841.0, y: 1190.0},
            },
            state: PlotterState::new(),
        }
    }
}

impl Plotter {
    pub fn home_all(&mut self) -> &'static str {
        self.state.location = Some(self.bounds.min);
        "G28"
    }

    pub fn set_feed_rate(&mut self, feed_rate: u64) -> String {
        self.state.feed_rate = Some(feed_rate);
        format!("G1 F{}", feed_rate)
    }

    pub fn travel_to(&mut self, p: &Point) -> String {
        self.state.location = Some(*p);
        format!("G0 X{:.3} Y{:.3}", p.x, p.y)
    }

    pub fn move_to(&mut self, p: &Point) -> String {
        self.state.location = Some(*p);
        format!("G1 X{:.3} Y{:.3}", p.x, p.y)
    }

    pub fn pen_down(&mut self) -> &'static str {
        self.state.pen = PenState::Down;
        // Plotter Pen Extend
        // "M401"

        // Prusa MK3
        "G1 Z0"
    }

    pub fn pen_up(&mut self) -> &'static str {
        self.state.pen = PenState::Up;
        // Plotter Pen Retract
        // "M402"
        
        // Prusa MK3
        "G1 Z20"
    }

    pub fn plot_path(&mut self, c: &Path) -> GCode {
        let mut g = GCode::new();
        
        // If the pen is down, or if the pen's state is not known, raise it.
        match self.state.pen {
            PenState::Up => {},
            _ => g.push(self.pen_up()),
        }

        let mut last = false;
        c.points.iter().for_each( |p| {
            let current = self.bounds.contains(&p);

            // This is either the first point in the set or the last point was out of bounds.
            // Either way travel to the current point and put the pen down.
            if !last && current {
                g.push(self.travel_to(&p));
                g.push(self.pen_down());
            }

            // The whole linear move is in-bound, so move.
            if last && current {
                g.push(self.move_to(&p));
            }

            // The move would take us out of bounds, so raise the pen and continue processing
            // points.
            if last && !current {
                g.push(self.pen_up());
            }

            last = current;
        });

        // If the pen is down, or if the pen's state is not known, raise it.
        match self.state.pen {
            PenState::Up => {},
            _ => g.push(self.pen_up()),
        }

        g
    }
}

#[derive(Clone, Debug)]
struct Document {
    header: GCode,
    body: GCode,
    footer: GCode,
}

impl Document {
    pub fn new() -> Self {
        Self {
            header: GCode::new(),
            body: GCode::new(),
            footer: GCode::new(),
        }
    }
}

impl Into<GCode> for Document {
    fn into(self) -> GCode {
        let mut gcode = GCode::new();

        gcode.extend(self.header);
        gcode.extend(self.body);
        gcode.extend(self.footer);

        gcode
    }
}

#[derive(Clone, Debug)]
pub struct GCodeBuilder {
    plotter: Plotter,
    layout: Layout,
    document: Document,
    header_override: Option<GCode>,
    footer_override: Option<GCode>,
    comments: bool,
    feed_rate: u64,
}

impl GCodeBuilder {
    pub fn new() -> Self {
        Self {
            plotter: Plotter::default(),
            layout: Layout::new(),
            document: Document::new(),
            header_override: None,
            footer_override: None,
            comments: true,
            feed_rate: 2000,
        }
    }

    pub fn with_comments(mut self) -> Self {
        self.comments = true;
        self
    }

    pub fn without_comments(mut self) -> Self {
        self.comments = false;
        self
    }

    pub fn with_feed_rate(mut self, feed_rate: u64) -> Self {
        self.feed_rate = feed_rate;
        self
    }

    pub fn with_plotter(mut self, p: Plotter) -> Self {
        self.plotter = p;
        self
    }

    pub fn with_header<G>(mut self, g: G) -> Self 
        where G: Into<GCode>
    {
        self.header_override = Some(g.into());
        self
    }

    pub fn with_footer<G>(mut self, g: G) -> Self 
        where G: Into<GCode>
    {
        self.footer_override = Some(g.into());
        self
    }

    pub fn with_layout(mut self, layout: Layout) -> Self {
        self.layout = layout;
        self
    }

    pub fn build(mut self) -> GCode {
        if self.comments {
            self.document.header.push("; Header");
            self.document.body.push("; Body");
            self.document.footer.push("; Footer");
        }

        self.document.header.extend(match self.header_override {
            Some(header) => header,
            None => DEFAULT_HEADER.into(),
        });
        self.document.header.push(self.plotter.set_feed_rate(self.feed_rate));

        for (i, view) in self.layout.into_iter().enumerate() {
            self.document.body.push(format!("; View #{}", i));
            self.document.body.extend(self.plotter.plot_path(&view.path(&self.plotter.bounds)));
        }

        self.document.footer.extend(match self.footer_override {
            Some(footer) => footer,
            None => DEFAULT_FOOTER.into(),
        });

        self.document.into()
    }
}

