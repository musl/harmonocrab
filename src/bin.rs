use harmonocrab_lib::{
    gcode::{GCodeBuilder, Plotter},
    geometry::{HarmonographParameters, Layout, Path, Point, Rect, View},
};
use log::debug;
use std::f64::consts::*;

fn main() {
    env_logger::init();
    debug!("main");

    // Header for MK3
    let header = r#"
M201 X1000 Y1000 Z200 E5000 ; sets maximum accelerations, mm/sec^2
M203 X200 Y200 Z12 E120 ; sets maximum feedrates, mm/sec
M204 P1250 R1250 T1250 ; sets acceleration (P, T) and retract acceleration (R), mm/sec^2
M205 X8.00 Y8.00 Z0.40 E4.50 ; sets the jerk limits, mm/sec
M205 S0 T0 ; sets the minimum extruding and travel feed rate, mm/sec
M107

M402 ; retract pen
G90 ; use absolite coordinates
G28 W ; home all
G21 ; units in mm
G90 ; use absolite coordinates
"#;

    // Footer for MK3
    let footer = r#"
G1 Z20 ; retract pen
G28 W ; home all
M84 ; turn off all steper drivers
"#;

    let mk3s = Plotter::new(
        60,    // 1mm/s
        12000, // 200 mm/s
        Rect {
            min: Point { x: 41.0, y: 18.0 },
            max: Point { x: 252.0, y: 211.0 },
        },
    );

    let n = 40_000;
    let mut layout = Layout::new();

    let params = &HarmonographParameters {
        n,
        d: 0.39,
        t_min: 0.0,
        t_max: 100.0 * TAU,
        f1: 0.933,
        f2: 0.62347,
        p1: 2.17,
        p2: 0.0,
    };
    let path = Path::generate(params);
    let rect = Rect {
        min: Point {
            x: 6.0 + 0.0,
            y: 0.0,
        },
        max: Point {
            x: 6.0 + 96.0,
            y: 96.0,
        },
    };
    layout.push(View::new(path, rect));

    let params = &HarmonographParameters {
        n,
        d: 0.4,
        t_min: 0.0,
        t_max: 100.0 * TAU,
        f1: 2.0,
        f2: 1.0,
        p1: 0.0,
        p2: 0.0,
    };
    let path = Path::generate(params);
    let rect = Rect {
        min: Point {
            x: 12.0 + 96.0,
            y: 0.0,
        },
        max: Point {
            x: 12.0 + 192.0,
            y: 96.0,
        },
    };
    layout.push(View::new(path, rect));

    let params = &HarmonographParameters {
        n,
        d: 0.4,
        t_min: 0.0,
        t_max: 100.0 * TAU,
        f1: 3.0,
        f2: 1.00333,
        p1: 3.12,
        p2: 0.0,
    };
    let path = Path::generate(params);
    let rect = Rect {
        min: Point {
            x: 6.0 + 0.0,
            y: 96.0,
        },
        max: Point {
            x: 6.0 + 96.0,
            y: 192.0,
        },
    };
    layout.push(View::new(path, rect));

    let params = &HarmonographParameters {
        n,
        d: 0.4,
        t_min: 0.0,
        t_max: 100.0 * TAU,
        f1: 4.0,
        f2: -1.2,
        p1: 0.0,
        p2: 0.0,
    };
    let path = Path::generate(params);
    let rect = Rect {
        min: Point {
            x: 12.0 + 96.0,
            y: 96.0,
        },
        max: Point {
            x: 12.0 + 192.0,
            y: 192.0,
        },
    };
    layout.push(View::new(path, rect));

    let gcode = GCodeBuilder::new()
        .with_plotter(mk3s)
        .with_header(header)
        .with_footer(footer)
        .with_layout(layout)
        .build();

    println!("{}", gcode);
}
