# harmonocrab
A tool to generate harmonograph-style curves and convert them to gcode for plotters or 3d printers with pen attachments.

## Prerequisites
- A reasonably recent rust nightly toolchain

## Building
- `cargo build`

## Running
- `cargo run > curves_for_your_plotter.gcode`

## Notes
- Always check the gcode for issues before running it on physical hardware. Use an online viewer, read the code. Mistakes and bugs could seriously damage your hardware.
